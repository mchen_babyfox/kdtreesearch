/*************************************************************
 * KD Tree Closest Point Search Demo
 * Written for Games Programming Modules
 * Author: Minsi Chen
 * Email: m.chen@hud.ac.uk
 * ***********************************************************/
#pragma once

#include <cmath>
#include <vector>
#include <ostream>

//Declare an enum type for more readable label for the coordinate axes
enum EKDTreeDim
{
    DIMX = 0,
    DIMY = 1,
    DIMZ = 2,
    DIMMAX = 3
};

//Begin: definition of a template Vector 3D class
//Please note: in C++ you can have more than one class declared/defined in a header and source.
//Althought, this is rather discouraged. It is done here to primarily show demostrate the templated KDTree search.
template <class T>
class Vector3
{
private:
  T xyz[3];

public:
  Vector3(T x = 0.0f, T y = 0.0f, T z = 0.0f)
  {
    SetVector(x,y,z);
  }

  Vector3(const Vector3& rhs)
  {
      xyz[0] = rhs.GetDim(DIMX);
      xyz[1] = rhs.GetDim(DIMY);
      xyz[2] = rhs.GetDim(DIMZ);
  }

  void SetVector(T x = 0.0f, T y = 0.0f, T z = 0.0f)
  {
    xyz[0] = x; xyz[1] = y; xyz[2] = z;
  }

  T GetDim(EKDTreeDim dim) const
  {
    return xyz[dim];
  }

  T DistanceSqr(const Vector3& rhs)
  {
    T x = xyz[0] - rhs.GetDim(DIMX);
    T y = xyz[1] - rhs.GetDim(DIMY);
    T z = xyz[2] - rhs.GetDim(DIMZ);

    return x*x + y*y + z*z;
  }

  T Distance(const Vector3& rhs)
  {
    T x = xyz[0] - rhs.GetDim(DIMX);
    T y = xyz[1] - rhs.GetDim(DIMY);
    T z = xyz[2] - rhs.GetDim(DIMZ);

    return sqrt(x*x + y*y + z*z);
  }

  friend std::ostream& operator<<(std::ostream& os, const Vector3& rhs)
  {
    os<<'('<<rhs.xyz[0]<<' '<<rhs.xyz[1]<<' '<<rhs.xyz[2]<<')';
    return os;
  }
};
//End: Vector 3

//Begin: Declare a templated KDTreeNode class
//KDTree is a binary tree which split multidimensional space using alternating spliting plan known as the key and median
template <class T, class K>
class KDTreeNode
{
public:

  KDTreeNode();
  KDTreeNode(EKDTreeDim dim);
  ~KDTreeNode();

  void SetChildren(KDTreeNode* less = NULL, KDTreeNode* greater = NULL);

  T Median;
  K Key;
  EKDTreeDim Dim;
  KDTreeNode* Less;
  KDTreeNode* Greater;
  std::vector<T> Data;
};
//End: KDTreeNode Declaration

//Begin: KDTree declaration
template <class T, class K>
class KDTree
{
public:
  KDTree();
  ~KDTree();

  void BuildKDTreeFromList(std::vector<T>& data, int threshold);
  T SearchClosest(T& query);

  inline const KDTreeNode<T,K>* GetRoot() const
  {
    return Root;
  }

private:

  void Clear();

  void Partition(KDTreeNode<T,K>* node, std::vector<T>& data, EKDTreeDim dim, int threshold);

  void FindClosestNode(KDTreeNode<T,K>* node, KDTreeNode<T,K>** bestnode, T& query, K& closest, EKDTreeDim dim);

  T FindMedianValue(std::vector<T>& data, std::vector<T>& lt, std::vector<T>& ge,  EKDTreeDim dim);

  KDTreeNode<T,K>* Root;
};
//End: KDTree declaration

//Include the implementation of Templated KDTreeNode and KDTree classes
//N.B. for a templated class, its definition should be in the header for the compiler preprocessor to performm substitution.
//This is one of the special case where the implementation of a class in placed in the header file.
#include "KDTree.hpp"
