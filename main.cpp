/*************************************************************
 * KD Tree Closest Point Search Demo
 * Written for Games Programming Modules
 * Author: Minsi Chen
 * Email: m.chen@hud.ac.uk
 * ***********************************************************/

#include <iostream>
#include <random>
#include <ctime>
#include "KDTree.h"

#define NUM_SAMPLES 1e6  //default sample is 1 million points
#define NUM_TRIALS 100

typedef Vector3<float> Vector3f;

//Brute-force search
Vector3f LinearSearchClosest(std::vector<Vector3f>& data, Vector3f& query)
{
  Vector3f result;
  float closest = 1.0e5f; // should be distant enough!
  auto it = data.cbegin();

  for(; it != data.cend(); it++)
  {
    float distance = query.DistanceSqr(*it);

    if(distance < closest)
    {
      closest = distance;
      result = *it;
    }
  }

  return result;
}

int main( int argc, char** argv )
{
  KDTree<Vector3f, float> PointKDTree;
  std::vector<Vector3f> PointSamples;
  std::vector<Vector3f> Duplicate;

  std::random_device rndDevice;
  std::mt19937 rndGenerator(rndDevice());
  std::uniform_real_distribution<float> distribution(-5.0f, 5.0f);

  std::cout<<"Generating "<<NUM_SAMPLES<<" random points"<<std::endl;
  for( int i = 0; i < NUM_SAMPLES; i++ )
  {
    Vector3f point(
      distribution(rndGenerator),
      distribution(rndGenerator),
      distribution(rndGenerator)
      );

    PointSamples.push_back(point);
    Duplicate.push_back(point);
  }

  std::cout<<"Building KDTree..."<<std::endl;
  //Build a KDTree from the given list
  //A greater threshold may result in a shallower tree and reduce build time
  PointKDTree.BuildKDTreeFromList(Duplicate, 1);

  for( int nTrials = 0; nTrials < NUM_TRIALS; nTrials++)
  {
    //Generate a random query point
    Vector3f query(
        distribution(rndGenerator),
        distribution(rndGenerator),
        distribution(rndGenerator)
        );

    clock_t start;
    clock_t elapsed_bf;
    start = clock();
    Vector3f closest = LinearSearchClosest(PointSamples, query);
    elapsed_bf = clock() - start;

    //Brute force distance result
    float closest_distance_bf = closest.Distance(query);
    std::cout<<"==================Trial "<<nTrials+1<<"=================="<<std::endl;
    std::cout<<"Linear search: "<<NUM_SAMPLES<<" points in "<<((float)elapsed_bf)/CLOCKS_PER_SEC<<"s"<<std::endl;
    std::cout<<"Linear Search: closest point to "<<query<<" is "
      <<closest<<" dist: "<<closest_distance_bf<<std::endl;

    start = clock();
    closest = PointKDTree.SearchClosest(query);
    clock_t elapsed_kd = clock() - start;

    //KD tree distance result
    float closest_distance_kd = closest.Distance(query);
    std::cout<<"KDTree search: "<<NUM_SAMPLES<<" points in "<<((float)elapsed_kd)/CLOCKS_PER_SEC<<"s"<<std::endl;
    std::cout<<"KDTree Search: closest point to "<<query<<" is "
      <<closest<<" dist: "<<closest_distance_kd<<std::endl;

    //We of course expect both search return identical results
    //Brute force result is the ground truth
    if(closest_distance_bf != closest_distance_kd)
    {
      std::cout<<"FAIL!!!"<<std::endl;
    }
    else
    {
      std::cout<<"PASS!!!"<<std::endl;
    }

    std::cout<<"Speed up factor (BF_time/KDT_time):"<<(float)elapsed_bf/elapsed_kd<<std::endl;

    std::cout<<"====================================================="<<std::endl;
  }
  return 0;
}
