# KDTree - closest point search demo

## Description:
The source code provided in this repository demonstrates two useful techniques as outlined below:

1. the use of template mechanism in C++ to implement a generic spatial search algorithm

2. the efficiency of searching for nearest neighbour (closest point) using KD-tree (originally proposed by Jon Bentley)

As the code uses lambda expressions, you need to compile this project using a C++ compiler that supports at least C++11 standard.

## About C++ template
Template is an important mechanism that allows us to design and implement generic programming interfaces.
It is used extensively in the STL (Standard Template Library) to provide generic data structure and algorithms.
In C++, both classes and functions can be templated.
Moreover, a template can by declared for types or values, e.g.

```cpp
template <class T, int dim>
class Vector {
...
	T elements[dim];
...
};
```
The above declares a templated vector class that allows both its element type and dimension to be specialised when used, e.g.

```cpp
typedef Vector<float, 3> Vector3f; //here we specialise it as a single precision 3D vector
typedef Vector<double, 4> Vector4d; //here we specialise it as a double precision 4D vector
```

As templates are resolved at compile time, we can also use template to create constants or constant functions, e.g. the following uses template to generate factorial of N at compile time.

```cpp
template <int N>
int Factorial()
{
	return N*Factorial<N-1>();
}

template <>
int Factorial<0>()
{
	return 1;
}

int main()
{
	int fact10 = Factorial<10>(); //fact10 should be 10!
	std::cout << "Factorial 10 is " << fact10 << std::endl;
}
```
The factorial example is a demonstration of simple metaprogramming, i.e. we asked the compiler to generate code for us.
We can use the same principle to generate code body for structs and classes as well, see [Template Metaprogramming in C++](https://www.geeksforgeeks.org/template-metaprogramming-in-c/).

## About KD-tree
A KD-tree is a binary tree that divides high-dimesional data ($$dim \ge 2$$) into smaller partitions using simple partitioning geometries.
Comparing to BSP (Binary Spatial Partitioning) where partition planes have an arbitrary orientation, the partitioning geometires in KD-tree are chosen to be orthogonal to each axis.
Since a KD-tree only divides data along one dimension at each level, it is more conveninent for paritioning higher dimension data. 
For more detailed discussion, see Jon Bentley's [original paper on KD-tree](https://dl.acm.org/doi/10.1145/361002.361007).