/*************************************************************
 * KD Tree Closest Point Search Demo
 * Written for Games Programming Modules
 * Author: Minsi Chen
 * Email: m.chen@hud.ac.uk
 * ***********************************************************/
#include <algorithm>
#include <queue>
#include "KDTree.h"

/*Start of KDTreeNode Definition*/
template <class T, class K>
KDTreeNode<T,K>::KDTreeNode()
{
  SetChildren();
}

template <class T, class K>
KDTreeNode<T,K>::KDTreeNode(EKDTreeDim dim)
{
  Dim = dim;

  SetChildren();
}

template <class T, class K>
KDTreeNode<T,K>::~KDTreeNode()
{
  Data.clear();
}
template <class T, class K>
void KDTreeNode<T,K>::SetChildren(KDTreeNode* less, KDTreeNode* greater)
{
  Less = less;
  Greater = greater;
}
/*End of KDTreeNode Definition*/

/*Start of KDTree Definition*/
template <class T, class K>
KDTree<T,K>::KDTree()
{
  Root = NULL;
}

template <class T, class K>
KDTree<T,K>::~KDTree()
{
  Clear();
}

template <class T, class K>
void KDTree<T,K>::BuildKDTreeFromList(std::vector<T>& data, int threshold)
{
  int size = data.size();

  if (Root == NULL)
  {
    Root = new KDTreeNode<T,K>(DIMX);

    Partition(Root, data, Root->Dim, threshold);
  }
}

template <class T, class K>
T KDTree<T,K>::SearchClosest(T& query)
{
  T result;

  KDTreeNode<T,K>* bestnode = NULL;
  K closest = 1e5f;

  FindClosestNode(Root, &bestnode, query, closest, Root->Dim);

  if(bestnode)
  {
    if(bestnode->Data.size() == 0) return bestnode->Median;

    //In case the leafnode threhold is greater than 1
    closest = 1e5f;

    auto it = bestnode->Data.cbegin();

    for(; it != bestnode->Data.cend(); it++)
    {
      K distance = query.DistanceSqr(*it);

      if(distance < closest)
      {
        closest = distance;
        result = *it;
      }
    }
  }

  return result;
}

template <class T, class K>
void KDTree<T,K>::FindClosestNode(KDTreeNode<T,K>* node, KDTreeNode<T,K>** bestnode, T& query, K& closest, EKDTreeDim dim)
{
  if(node == NULL)
    return;

  K dist = query.DistanceSqr(node->Median);
  K dDim = node->Median.GetDim(dim) - query.GetDim(dim);
  K dDim2 = dDim*dDim;

  if(node->Data.size() > 0) //we have more data to examine here
  {
    auto it = node->Data.cbegin();

    for(; it != node->Data.cend(); it++)
    {
      K distance = query.DistanceSqr(*it);

      if(distance < dist)
      {
        dist = distance;
      }
    }
  }

  if(dist < closest)
  {
    closest = dist;
    *bestnode = node;
  }

  if(!dist) return;

  EKDTreeDim nextDim = (EKDTreeDim)((dim+1)%DIMMAX);

  FindClosestNode(dDim > 0?node->Less:node->Greater, bestnode, query, closest, nextDim);

  if(dDim2 >= closest) return;

  FindClosestNode(dDim > 0?node->Greater:node->Less, bestnode, query, closest, nextDim);
}


template <class T, class K>
void KDTree<T,K>::Clear()
{
  //A demonstration of breadth first traversal to clear the tree
  std::cout<<"Clean up KD Tree"<<std::endl;
  std::queue<KDTreeNode<T,K>*> cleanupQ;

  cleanupQ.push(Root);

  while(!cleanupQ.empty())
  {
    KDTreeNode<T,K>* head = cleanupQ.front();
    cleanupQ.pop();

    if(head != NULL)
    {
      cleanupQ.push(head->Less);
      cleanupQ.push(head->Greater);
      delete head;
    }
  }
}

template <class T, class K>
T KDTree<T,K>::FindMedianValue(std::vector<T>& data, std::vector<T>& lt, std::vector<T>& ge, EKDTreeDim dim)
{
  T value;

  std::sort(data.begin(), data.end(), [=](T& lhs, T& rhs){ return lhs.GetDim(dim) < rhs.GetDim(dim);});

  int size = data.size();

  if(size == 2)
  {
    lt.push_back(data[0]);
    ge.push_back(data[1]);
    return data[1];
  }

  int mid = size >> 1;
  value = data[mid];

  int ltsize = mid;
  int gesize = size%2 == 0 ? mid-1:mid;

  lt.resize(ltsize);

  ge.resize(gesize);

  std::copy(std::begin(data), std::begin(data)+ltsize, std::begin(lt));
  std::copy(std::end(data)-gesize, std::end(data), std::begin(ge));

  data.clear();

  return value;
}

template <class T, class K>
void KDTree<T,K>::Partition(KDTreeNode<T,K>* node, std::vector<T>& data, EKDTreeDim dim, int threshold)
{
  int size = data.size();

  EKDTreeDim nextDim = (EKDTreeDim)((dim+1)%DIMMAX);

  //This node has met its threshold, no further partitioning required
  if(size <= threshold)
  {
    node->Median = data[0];
    node->Data = data;
    return;
  }

  //Further partition is still possible
  std::vector<T> lt; //less than set
  std::vector<T> ge; //greater or equal set

  T median = FindMedianValue(data, lt, ge, dim);

  //Partition the data along the given dim
  node->Key = median.GetDim(dim);
  node->Median = median;

  //Create two new children
  node->Less = new KDTreeNode<T,K>(nextDim);
  node->Greater = new KDTreeNode<T,K>(nextDim);

  Partition(node->Less, lt, nextDim, threshold);
  Partition(node->Greater, ge, nextDim, threshold);
}

/*End of KDTree Definition*/
